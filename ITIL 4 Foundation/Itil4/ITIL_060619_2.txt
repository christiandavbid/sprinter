GESTION DE LA IMPLEMENTACION (DEPLOY-DESPLIEGUE)
mover hw, sw, doc, procesos a entornos en vivo (de prueba a produccion)| Enfoques: Despliegue por fases, entrega continua, despliegue big bang, despliegue pull. | Despliegue bigbang-> salen todos los modulos a la vez | Entrega continua-> se va desplegando de acuerdo a la necesidad de la empresa| Despliegue Pull-> 
Entorno de Prueba a Produccion.  

El prop�sito de la pr�ctica de administraci�n de la implementaci�n es mover hardware, software, documentaci�n, procesos o cualquier otro componente nuevo o modificado a entornos activos

GESTION DE LANZAMIENTO (RELEASE)
publicacion de un servicio o configuracion, que este disponible para su uso. | liberar el software | llamar al cliente para coordinar con el cliente para que el ERP salga a produccion | coordino pendientes para un nuevo  
Servicio ya liberado | Tiempo de transicion y monitoreo | tener un plan de soporte luego | 

El prop�sito de la pr�ctica de administraci�n de lanzamientos es hacer que los servicios y caracter�sticas nuevos y modificados est�n disponibles para su uso


............................................................

GESTION DE INCIDENCIAS
Incidente->interrupcion no planificada de un servicio |la reduccion de la calidad de un servicio | Minimizar el impacto negativo mediante la restauracion del funcionamiento normal del servicio lo mas rapido posible | establcer prioridad de los incidentes (bajo, medio y alto)| TIPOS: {resuelto por el propio usuario} {resuleto por mesa de ayuda} {escalado a un equipo de soporte} {escalado a proveedores o socios} {pueden requerir un equipo temporal para trabajar juntos} {se puede invocar planes de recuperacion de desastres} | La prioridad de acuerdo a los SLA | Establecer los estados de los incidentes (Abierto, en proceso, atendido, solucionado o cerrado) | Podria hacer prueba previa antes del help desk | 
Alineado al requerimiento del cliente | Variables: Cantidad de incidentes que vas atender en promedio |

*/Los tiempos de resoluci�n objetivo se acuerdan, documentan y comunican para garantizar que las expectativas sean realistas �. Una buena herramienta de administraci�n de servicios de TI puede ayudar a la organizaci�n a cumplir estos tiempos, pero la herramienta no puede garantizar que esto suceda. Adem�s, identificar las causas de los incidentes es una actividad de 'gesti�n de problemas'.

Las herramientas modernas de gesti�n de servicios de TI pueden proporcionar una correspondencia autom�tica de incidentes con otros incidentes, problemas o errores conocidos �.

�Los incidentes m�s complejos, y todos los incidentes mayores, a menudo requieren un equipo temporal para trabajar juntos para identificar la resoluci�n�. �La investigaci�n de incidentes m�s complicados a menudo requiere conocimiento y experiencia, en lugar de pasos de procedimiento. /*

GESTION DE PROBLEMAS
Causa potencial de uno o mas incidentes | Cuando un incidente pasa muchas veces puede convertirse en un problema | Gestion de problemas se encarga de la gestion cuando la incidencia pasa muchas veces | Diagnostico: ubicar el error y dar solucion temporal | todo esto debe estar en la base de datos de conocimiento-> CMDBS (gestion de la configuracion) | Proposito: Reducir la probabilidad y el impacto de los incidentes mediante la identificacion de las causas reales y potenciales de los incidentes, y la gestion de soluciones y errores conocidos.
*Fases-> {Problema de identificacion -> identificacion del problema} {Control de problemas -> causa del problema} {Control de errores donde registro todos los errores y las soluciones temporales}.
Contribucion a la cadena de valor:
-Reduccion de incidentes (mejora)
-participacion de interesados


El prop�sito de la pr�ctica de gesti�n de problemas es reducir la probabilidad y el impacto de los incidentes mediante la identificaci�n de las causas reales y potenciales de los incidentes ".

*requerir catalogo de servicios es una incidencia, porque debe estar publicado siempre.

GESTION DE REQUERIMIENTOS (SOLICITUD DE SERVICIO)
Solicitud de un usuario que inicia una accion de servicio segun el acuerdo normal.
*Respecto a la Solicitud de Servicios-> hace referencia a un cambio estandar (ejemplo: no requiere autorizacion: cambiar contrase�a del correo) | la mejor manera de mejorar un servicio es el requerimiento del usuario | 
Tipos de Solicitud->{accion de prestacion de servicio}{de informacion}{comentario, felicitaciones y quejas} {acceso a un recuerso o servicio}{provision de un recurso o servicio} |
Pauta para la solicitud de servicios-> Formato estandar para notificar la solicitud. | cadena de valor: canal para iniciativas de mejora; comunicacion con las partes interesadas; procesos para atender los requerimientos.
*/El prop�sito de la pr�ctica de gesti�n de solicitudes de servicio es respaldar la calidad acordada de un servicio manejando todas las solicitudes de servicio predefinidas e iniciadas por el usuario de una manera efectiva y f�cil de usar. "La administraci�n de solicitudes de servicio no proporciona un �nico punto de contacto para usuarios de servicios/*

MESA DE SERVICIO (HELP DESK)
Captar la demanda de resolucion de incidentes y requerimientos. | Canal de entrada para que los incidentes se atiendan en aal gestion de problemas. | Cual de las practicas requiere conocimiento de atencion al cliente | Canales de entrada a la help desk: llamadas telf, portales de servicios, chat en vivo, email, mesa de servicio sin previa cita, redes sociales, chatbots como wish |  Tipos de mesa de ayuda: {service desk local}-{services desk centralizado ejemplo desde miraflores puedo atender a muchos distritos y cuando no puedo solucionarlo envio a un tecnico, el mas economico y utilizado}-{services desk virtual, el que utiliza la tecnologia, ejemplo: una central telefonica (PDX) y derivar llamada a diferentes equipo, pero los equipos deben tener el mismo sistema}-{service desk siguiendo al sol, para atender las 24 horas en horario de oficina, service desk en diferentes paises, 3 equipos o paises en horario de oficina segun su horario regional, ejemplo: Peru, espa�a y china}

� Otro aspecto clave de una buena mesa de servicio es su comprensi�n pr�ctica.
de la organizaci�n m�s amplia, los procesos de negocio y los usuarios.
