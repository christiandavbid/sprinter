@ECHO OFF
@ECHO Actualizando Sprinter ...
@Copy %~d0%~p0\librerias\flexcell.ocx c:\windows\system32
@Copy %~d0%~p0\librerias\mscomctl.ocx c:\windows\system32
@Copy %~d0%~p0\librerias\pdf417.dll c:\windows\system32
@Copy %~d0%~p0\librerias\itextsharp.dll c:\windows\system32
@Copy %~d0%~p0\librerias\zxing.dll c:\windows\system32
@Copy %~d0%~p0\librerias\chilkatax-9.5.0-win32.dll c:\windows\system32
@c:
@cd \windows\microsoft.net\framework\v4.0.30319
@regasm c:\windows\system32\pdf417.dll /register /codebase
@regasm c:\windows\system32\itextsharp.dll /register /codebase
@regsvr32 c:\windows\system32\flexcell.ocx
@regsvr32 c:\windows\system32\mscomctl.ocx
@regsvr32 c:\windows\system32\chilkatax-9.5.0-win32.dll
@exit