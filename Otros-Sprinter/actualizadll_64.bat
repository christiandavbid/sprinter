@ECHO OFF
@ECHO Actualizando Sprinter ...
@Copy %~d0%~p0\librerias\flexcell.ocx c:\windows\syswow64
@Copy %~d0%~p0\librerias\pdf417.dll c:\windows\syswow64
@Copy %~d0%~p0\librerias\zxing.dll c:\windows\syswow64
@Copy %~d0%~p0\librerias\mscomctl.ocx c:\windows\syswow64
@Copy %~d0%~p0\librerias\chilkatax-9.5.0-win32.dll c:\windows\syswow64
@c:
@cd \windows\microsoft.net\framework\v4.0.30319
@regasm c:\windows\syswow64\pdf417.dll /register /codebase
@regsvr32 c:\windows\syswow64\flexcell.ocx
@regsvr32 c:\windows\syswow64\mscomctl.ocx
@regsvr32 c:\windows\syswow64\chilkatax-9.5.0-win32.dll
@exit