Programación PHP
-Programación .NET
-Conocimientos en SQL y MySQL
-Conocimientos en Fortinet
-Conocimientos en cableado estructurado
-Conocimientos en Active Directory.
-Conocimientos en Servidor DHCP, Terminal Server, NTFS, WEB.
-Conocimientos en ERP.
-Conocimientos en Central Telefónica.
-Conocimientos en configuración de AP y switch.
-Soporte a usuario final.

Conocimiento y experiencia en Sistemas operativos (plataformas AIX, Windows, GNU/LINUX y MainFrame)
Conocimiento y experiencia en Redes (LAN, WAN)
Conocimientos de programación Java, .Net, Python
Conocimiento y administración de Base de datos: Oracle, SQL, DB2, Mongo DB y Cassandra (que conozca cualquier de estos, deseable).
Deseable  conocimientos en Ethical Hacking y/o Pentesting.
Deseable conocimientos en Cloud Security, Data, Encriptación, desarrollo seguro
Deseable Certificaciones en: CISM, CEH, CPTE, CISSP o ISO 27001

Experiencia con Servidores de Aplicaciones (IIS, JBoss, Weblogic), Servidores Web y Sistemas Operativos Windows Server y Linux.
Conocimiento en despliegue e instalación de aplicaciones Java J2EE, .NET y/o C.
Conocimiento de administración y operación de BD MS SQL Server y/o Oracle.